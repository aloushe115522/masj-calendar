<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->
            onDelete('cascade')->onUpdate('cascade');   
            $table->bigInteger('calendar_id')->unsigned();;
            $table->foreign('calendar_id')->references('id')->on('calendars')->
            onDelete('cascade')->onUpdate('cascade'); 
            $table->enum('repeat', ['no_repeat', 'daily','weekly','monthly','yearly','quarterly']);
            $table->dateTime('datetime');
            $table->string('color');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
