<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Calendar;
use App\Models\User_Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
  
    public function index()
    {
        $user=Auth::user();
        return response()->json($user->calendars,200);
    }

    public function store(Request $request)
    {
        $request->validate([
              'name'    => ['required'],
              'color'   => ['required'],
        ]);
        $user=Auth::user();
        $calendar = new Calendar();
        $calendar->name = $request->name;
        $calendar->owner_id = $request->user()->id;
        $calendar->color = $request->color;
        $calendar->save();
        to_user($user)->calendars()->attach($calendar->id, ['permission' => 'owner' ]);
        return response()->json($calendar, 200);
    }

    public function show($id)
    {
        $user=to_user(Auth::user());
        $calendar = Calendar::findOrFail($id);
        $permission=$user->permissionsTo($id);
        if(!$permission)
            return response()->json(403);
            
        return response()->json($calendar, 200);
    }

    public function update(Request $request, $id)
    {
        $user=to_user(Auth::user());
        $calendar = Calendar::findOrFail($id);
        $permission=$user->permissionsTo($id);
        if(!$permission)
            return response()->json(403);

        if (in_array($permission,['owner','edit'])) {
            $calendar->name = $request->name;
            $calendar->color = $request->color;
            $calendar->save();
            return response()->json($calendar, 200);
        }
        return response()->json(403);
    }

    public function destroy($id)
    {
        $user=to_user(Auth::user());
        $calendar = Calendar::findOrFail($id);
        $permission=$user->permissionsTo($id);
        if(!$permission)
            return response()->json(403);

        if (in_array($permission,['owner','edit'])) {
            $calendar->delete();
            return response()->json($calendar, 200);
        }
        return response()->json(403);
    }
}
