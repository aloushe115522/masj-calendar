<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Calendar;
use App\Models\Event;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'calendars' => 'array',
        ]);
        $user = Auth::user();
        $calendars_ids = to_user($user)->calendars()->pluck('calendars.id')->toArray();

        $_calendars_ids = [];

        if ($request->calendars) {
            foreach ($calendars_ids as $calendar_id) {
                if (in_array($calendar_id, $request->calendars)) {
                    $_calendars_ids[] = $calendar_id;
                }
            }
        } else {
            $_calendars_ids = $calendars_ids;
        }
        $events = [];
        $all_events = Event::whereIn('calendar_id', $_calendars_ids)->with('calendar')->get();
        foreach ($all_events as $e) {
            if (!$request->year) {
                $events[] = $e;
            } else if ($request->month) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $e->datetime);
                $repeated = $this->is_repeat($e->id);
                if ($repeated)
                    $events[] = $repeated;

                if ($date->month == $request->month && $date->year == $request->year && !$repeated) {
                    $events[] = $e;
                }
            }
        }
        return response()->json($events, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required'],
            'repeat' => ['required', 'in:no_repeat,daily,weekly,monthly,yearly,quarterly'],
            'datetime' => ['required', ' date_format:Y-m-d\TH:i'],
            'color' => 'required',
            'calendar_id' => 'required',
        ]);

        $user = Auth::user();
        $calendar = Calendar::findOrFail($request->calendar_id);
        $user_calendar = to_user($user)->calendars()->where('calendar_id', $request->calendar_id)->first();

        if (!$user_calendar) {
            return response()->json(403);
        }

        $user_permissions = $user_calendar->pivot->permission;
        if ($user_permissions == 'owner' || $user_permissions == 'edit') {
            $event = new Event();
            $event->title = $request->title;
            $event->description = $request->description;
            $event->repeat = $request->repeat;
            $event->datetime = $request->datetime;
            $event->color = $request->color;
            $event->user_id = $request->user()->id;
            $event->calendar_id = $request->calendar_id;
            $event->save();
            return response()->json($event, 200);

            $notification = new Notification();
            $notification->user_id = $request->user()->id;
            $notification->event_id = $event['id'];
            $notification->seen = 'false';
            $notification->sent = 'false';
            $notification->save();
        }
        return response()->json(403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        $user = to_user(Auth::user());
        if (!$user->permissionsTo($event->calendar->id)) {
            return response()->json(403);
        }
        return response()->json($event, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $user = to_user(Auth::user());
        if (in_array($user->permissionsTo($event->calendar->id), ['owner', 'edit'])) {
            $event->title = $request->title;
            $event->description = $request->description;
            $event->repeat = $request->repeat;
            $event->datetime = $request->datetime;
            $event->color = $request->color;
            $event->save();
            return response()->json($event, 100);
        }
        return response()->json(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $user = to_user(Auth::user());
        if (in_array($user->permissionsTo($event->calendar->id), ['owner', 'edit'])) {
            $event->delete();
            return response()->json(200);
        }
        return response()->json(403);
    }

    public function is_repeat($id)
    {
        $event = Event::where('id', $id)->first();
        $current_date = Carbon::now();
        if ($event->repeat == 'yearly') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $event->datetime);
            if ($date->month == $current_date->month) {
                $date->year = $current_date->year;
                $event->datetime = $date;
                return $event;
            }
        }
        if ($event->repeat == 'monthly') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $event->datetime);
            $date->month = $current_date->month;
            $date->year = $current_date->year;
            $event->datetime = $date;
            return $event;
        }
        if ($event->repeat == 'quarterly') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $event->datetime);
            $months = $current_date->diffInMonths($date);
            if ($months % 3 == 0) {
                $date->month = $current_date->month;
                $date->year = $current_date->year;
                $event->datetime = $date;
                return $event;
            }
        }
        if ($event->repeat == 'weekly') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $event->datetime);
            $events = array();
            $day = 0;
            for ($i = 1; $i < $current_date->daysInMonth + 1; $i++) {
                $current_date->day = $i;
                if ($current_date->dayOfWeek == $date->dayOfWeek) {
                    $day = $i;
                    break;
                }
            }
            if ($day != 0) {

                $date->month = $current_date->month;
                $date->year = $current_date->year;
                $date->day = $day;
                $event->datetime = $date;
                array_push($events, $event);

                while ($day+7 < $current_date->daysInMonth) {
                    $date = Carbon::createFromFormat('Y-m-d H:i:s', $event->datetime);
                    $day = $day + 7;
                    $date->day = $day;
                    $temp = new Event();
                    $temp->title = $event->title;
                    $temp->description = $event->description;
                    $temp->repeat = $event->repeat;
                    $temp->color = $event->color;
                    $temp->user_id = $event->user_id;
                    $temp->calendar_id = $event->calendar_id;
                    $temp->datetime = $date;

                    array_push($events, $temp);
                }
                return $events;
            }
        }
    }
}
