<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Laravel\Socialite\Facades\Socialite;
// use Socialite;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Models\User;
use App\Models\User_Calendar;
use Prophecy\Call\Call;

class AuthController extends Controller
{
        public function login(Request $request)
    {
        $request->validate([
            'id_token' => ['required'],
        ]);
        $response = Http::get("https://oauth2.googleapis.com/tokeninfo?id_token=".$request->id_token);
        if($response->status()!=200)
            throw new BadRequestException();
        $user =User::where('email', $response['email'])->first();
        if (!$user) {
            $user = new User;
            $user->name = $response['name'];
            $user->image = $response['picture'];
            $user->email = $response['email'];
            $user->save();

            $calendar = new Calendar();
            $calendar->name = $response['name'];
            $calendar->owner_id = $user['id'];
            $calendar->save();

            $user->calendars()->attach($calendar->id, ['permission' => 'owner' ]);

            $access_token= $user->createToken('Sanctum')->plainTextToken;
            return response()->json([
                'access_token'=>$access_token,
                'user' => $user,
                'calendar' => $calendar,
            ], 200);
        }
        $access_token= $user->createToken('Sanctum')->plainTextToken;
        return response()->json([
            'access_token'=>$access_token,
            'user' => $user,
        ], 200);
    }
}
