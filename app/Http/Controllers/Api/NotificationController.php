<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function storeToken(Request $request)
    {
        $user=to_user(Auth::user());
        $user->fcm_token=$request->token;
        $user->save();
        return response()->json(['Token successfully stored.']);
    }

    public function sendNotification(Request $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $serverKey = 'AAAA0ISykMk:APA91bFX8ezGyX4KVKw86NizvRyZGRBcHT4tMz-CgoIkE8VdfGr5jz2myyMtdc-WyaavXCF4p7k_oN8FdnOU9hNpGDye1kgjkDZRcJNFprB98sHvQXf13UE9ZojFEoKG1HaLXvnJ-_Yv';

        $msg = [
            'title' => $request->title,
            'body' => $request->body,
            'icon' => 'myicon',
             'sound' => 'mySound'
          ];

          $fields = [
            'to' =>  $request->fcm_Token,
            'notification' => $msg
          ];

          $headers = [
            'Authorization: key=' . $serverKey,
            'Content-Type: application/json'
          ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode( $fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            dd('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        // FCM response
        return response()->json($result);

    }
}
