<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'user_id',
        'owner_id',
        'color',
    ];

    public function users(){
    return $this->belongsToMany(User::class, 'user_calendars','calendar_id','user_id');
    }

    public function events(){
        return $this->hasMany(Event::class);
    } 
}
