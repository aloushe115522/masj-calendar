<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'desecription',
        'calendar_id',
        'user_id',
        'repeat',
        'datetime',
        'color'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function calendar(){
        return $this->belongsTo(Calendar::class);
    }

    public function notifications(){
        return $this->hasMany(Notification::class);
    }

}
